#ifndef DECTREE_H
#define DECTREE_H
#include <vector>

class DecTree
{
public:
    DecTree(int chekedAtribute, double splitPoint, bool discrete, bool isEnd, bool answers);
    bool PredictOutcome(double* data);
    void addChild(DecTree* child);
private:
    bool Split(double data);
    bool end;
    bool answer;
    double point;
    bool discrete;
    int chekedAtribute;
    std::vector<DecTree*> child;
};

#endif // DECTREE_H

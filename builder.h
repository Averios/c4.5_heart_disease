#ifndef BUILDER_H
#define BUILDER_H
#include "dectree.h"
#include <thread>

class Builder
{
public:
    Builder(int colSize);
    void SetUsedCol(bool* used);
    void SetDiscretion(bool *discrete);
    void SetData(std::vector<double*>* data);
    void SetDeterminedValue(double* vals);
    void SetRoot(DecTree* root);
    void SetColSize(int colSize);
    void SetParentDominant(bool ans);
    void SetCategoriesSize(int* Categories);
    void BuildTree();
    DecTree* Join();
private:
    double GetInfo();
    double GetGainInfo(int col);
    void StartBuilding();
    double GetSplitGain(int col, int attr);
    bool SplitData(const double* row);

    int colSize;
    int* Categories;
    int*** catCount;
    bool* discrete;
    std::vector<double*>* Data;
    bool* usedData;
    double* preDeterminedValue;
    double info;
    double* bestSplit;
    bool dominantAnswer;
    bool parent;
    int allSuccess;
    int allFail;
    std::thread t;
    DecTree* node;
};

#endif // BUILDER_H

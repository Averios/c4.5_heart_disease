#include <iostream>
#include <random>
#include <vector>
#include "datareader.h"
#include "builder.h"

int main()
{
    DataReader reader;
    DataReader reader2;
    reader.ReadFile("training.data", ',');
    reader2.ReadFile("test.data", ',');

//    std::vector<double*>* data = reader2.GetData();
//    int size = (*data).size();
//    double* now;
//    for(int i = 0; i < size; i++){
//        now = (*data)[i];
//        for(int j = 0; j < 14; j++){
//            std::cout << now[j] << " ";
//        }
//        std::cout << "\n";
//    }

    bool discrete[] = {false, true, true, false, false, true, true, false, true, false, true, true, true, false};
    int categories[] = {2, 2, 4, 2, 2, 2, 3, 2, 2, 2, 3, 4, 3, 2};
    bool* used = new bool[14];
    for(int i = 0; i < 14; i++){
        used[i] = true;
    }
    used[13] = false;
    double* predetermined;
    std::vector<double*> determined;
    double* max = reader.GetMaxValue();
    double* min = reader.GetMinValue();
    double tempAcc;

    std::vector<double> accuracy;
    std::vector<DecTree*> tree;
    std::vector<Builder*> builder;
    std::random_device rd;
    std::mt19937 en(rd());

    Builder* build;

    int totalTree;
    std::cout << "Enter the number of tree you want to make : ";
    std::cin >> totalTree;

    for(int i = 0; i < totalTree; i++){
        predetermined = new double[14];
        for(int j = 0; j < 13; j++){
            if(discrete[j]){
                predetermined[j] = -1;
            }
            else{
//                std::cout << j << " Max: " << max[j] << " Min: " << min[j] << "\n";
                double maxNow = max[j];
                double minNow = min[j];
                std::uniform_real_distribution<> rand(minNow, maxNow);
                predetermined[j] = (double)rand(en);
            }
        }
        predetermined[13] = 0;

        determined.push_back(predetermined);
//        std::cout << predetermined[i][0] << " " << predetermined[i][3] << " " << predetermined[i][4] << " " << predetermined[i][7] << " " << predetermined[i][9] << "\n";
        build = new Builder(14);
        build->SetColSize(14);
        build->SetDiscretion(discrete);
        build->SetDeterminedValue(predetermined);
        build->SetUsedCol(used);
        build->SetCategoriesSize(categories);
        build->SetData(reader.GetData());
        build->BuildTree();
//        std::cout << "Finish build\n";
        builder.push_back(build);
    }

    DecTree* temp1;
    Builder* now;
    for(int i = 0; i < totalTree; i++){
        now = builder[i];
        temp1 = now->Join();
        tree.push_back(temp1);
    }
    DecTree* uh;

    std::vector<double*>* data = reader2.GetData();
    int size = data->size();
    double* then;
    bool realAnswer;
    int success;
    bool predictedRes;
    for(int k = 0; k < totalTree; k++){
        uh = tree[k];
        success = 0;
        std::cout << "Tree " << k << "\n";
        for(int i = 0; i < size; i++){
            then = (*data)[i];
            realAnswer = then[13] > 0;
            predictedRes = uh->PredictOutcome(then);
            if(realAnswer == predictedRes){
                success++;
            }
//            std::cout << i << " " << predictedRes << " " << then[13] << "\n";
        }
        tempAcc = ((double)success / (double)size) * 100;
        accuracy.push_back(tempAcc);
        std::cout << tempAcc << "%\n";
    }

    return 0;
}

#include "datareader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

DataReader::DataReader(int colSize):
    colSize(colSize)
{
    for(int i = 0; i < colSize; i++){
        ColPoint.push_back(new std::vector<double>);
    }
    maxValue = new double[colSize];
    minValue = new double[colSize];
}


void DataReader::ReadFile(std::string path, char delim){
    std::ifstream file(path);
    std::string line;
    double* temp;

    std::getline(file, line);
    temp = SplitNumber(&line, delim);
    Data.push_back(temp);
    for(int i = 0; i < colSize; i++){
        maxValue[i] = minValue[i] = temp[i];
    }

    while(std::getline(file, line)){
        temp = SplitNumber(&line, delim);
        Data.push_back(temp);
        for(int i = 0; i < colSize; i++){
            if(temp[i] > maxValue[i]){
                maxValue[i] = temp[i];
            }
            else if(temp[i] < minValue[i]){
                minValue[i] = temp[i];
            }
        }
    }

    this->Normalize();
}

double* DataReader::SplitNumber(std::string *line, char delim){
    std::stringstream ss(*line);
    std::string item;
    double* temp = new double[colSize];
    for(int i = 0; i < colSize; i++){
        std::vector<double>* now = ColPoint[i];
//        std::cout << item << "\n";
        std::getline(ss, item, delim);
        if(item.find('?') != std::string::npos){
            temp[i] = -1;
        }
        else{
//            std::cout << item << " ";
            temp[i] = std::stod(item);
        }
        now->push_back(temp[i]);
    }

    return temp;
}

int DataReader::GetColSize(){
    return colSize;
}

int DataReader::GetRowSize(){
    return Data.size();
}

std::vector<double*>* DataReader::GetData(){
    return &Data;
}

std::vector<std::vector<double>*>* DataReader::GetColSeperated(){
    return &ColPoint;
}

double* DataReader::GetRowAt(int row){
    return Data[row];
}

double DataReader::GetDataAt(int row, int col){
    double* temp = Data[row];
    return temp[col];
}

void DataReader::Normalize(){
    int size = Data.size();
    double* temp;
    for(int i = 0; i < size; i++){
        temp = Data[i];

        temp[2]--;
        temp[10]--;
        if(temp[12] == 3.0f){
            temp[12] = 0;
        }
        else if(temp[12] == 6.0f){
            temp[12] = 1;
        }
        else{
            temp[12] = 2;
        }
    }
}

double* DataReader::GetMaxValue(){
    return this->maxValue;
}

double* DataReader::GetMinValue(){
    return this->minValue;
}

#include "dectree.h"

DecTree::DecTree(int chekedAtribute, double splitPoint, bool discrete, bool isEnd = false, bool answers = false)
{
    this->point = splitPoint;
    this->discrete = discrete;
    this->chekedAtribute =chekedAtribute;
    this->end = isEnd;
    if(end){
        this->answer = answers;
    }
}

bool DecTree::Split(double data){
    if(discrete){
        return data == point;
    }
    return data > point;
}

bool DecTree::PredictOutcome(double* data){
    if(end){
        return answer;
    }
    DecTree* next;
    if(Split(data[chekedAtribute])){
        next = child[1];
    }
    else{
        next = child[0];
    }
    return next->PredictOutcome(data);
}

void DecTree::addChild(DecTree *child){
    this->child.push_back(child);
}

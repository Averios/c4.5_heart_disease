#include "builder.h"
#include <cmath>
#include <iostream>

Builder::Builder(int colSize = 14)
{
    this->colSize = colSize;
//    catCount = nullptr;
}

void Builder::SetUsedCol(bool *used){
    this->usedData = used;
}

void Builder::SetDiscretion(bool *discrete){
    this->discrete = discrete;
}

void Builder::SetData(std::vector<double *> *data){
    this->Data = data;
}

void Builder::SetDeterminedValue(double *vals){
    this->preDeterminedValue = vals;
}

void Builder::SetRoot(DecTree *root){
    this->node = root;
}

void Builder::SetColSize(int colSize){
    this->colSize = colSize;
}

void Builder::SetParentDominant(bool ans){
    this->parent = ans;
}

void Builder::BuildTree(){
    t = std::thread(&Builder::StartBuilding, this);
//    StartBuilding();
}

DecTree* Builder::Join(){
    t.join();
//    std::cout << "Joined\n";
    return node;
}

double Builder::GetInfo(){
    allFail = allSuccess = 0;
    catCount = new int**[colSize];
    for(int i = 0; i < colSize; i++){
        if(discrete[i]){
            catCount[i] = new int*[Categories[i]];
            for(int j = 0; j < Categories[i]; j++){
                catCount[i][j] = new int[2];
                catCount[i][j][0] = catCount[i][j][1] = 0;
            }
        }
        else{
            catCount[i] = new int*[2];
            catCount[i][0] = new int[2];
            catCount[i][1] = new int[2];
            catCount[i][0][0] = catCount[i][1][0] = catCount[i][0][1] = catCount[i][1][1] = 0;
        }
    }
    int size = Data->size();

    for(int i = 0; i < size; i++){
        double* temp = (*Data)[i];
        int place = 0;
        if(temp[colSize - 1] > 0) place = 1;

        if(place == 1)allSuccess++;
        else allFail++;

        for(int j = 0; j < colSize; j++){
            if(temp[j] > -1 && usedData[j]){
                if(discrete[j]){
                    catCount[j][(int)temp[j]][place]++;
                }
                else{
                    if(temp[j] > preDeterminedValue[j]){
                        catCount[j][1][place]++;
                    }
                    else{
                        catCount[j][0][place]++;
                    }
                }
            }
        }
    }

    double div1 = (double) allFail / (double) Data->size();
    double div2 = (double) allSuccess / (double) Data->size();

    double temp2 = 0;
    if(div1 > 0){
        temp2 += div1 * log2(div1);
    }
    if(div2 > 0){
        temp2 += div2 * log2(div2);
    }

    dominantAnswer = allSuccess > allFail;
    return -temp2;
}

double Builder::GetGainInfo(int col){
    int** cols = catCount[col];

    double tempGain = 0;
    int bestAttr = 0;
    int all = 0;
    for(int i = 0; i < Categories[col]; i++){

        all += cols[i][0] + cols[i][1];
    }
    double tmp;
    for(int i = 0; i < Categories[col]; i++){
        tmp = ( info - GetSplitGain(col, i) ) * (double) all / (double)Data->size();
        if(tmp > tempGain){
            bestAttr = i;
            tempGain = tmp;
        }
    }
//    if(tempGain == 0){
//        std::cout << info << " " << GetSplitGain(col, bestAttr) << "\n";
//        std::cout << allFail << " " << allSuccess << "\n";
//        std::cout << cols[bestAttr][0] << " " << cols[bestAttr][1] << "\n";
//        std::cout << all << " " << Data->size() << "\n";
//    }

    if(discrete[col]){
        bestSplit[col] = (double)bestAttr;
    }
    else{
        bestSplit[col] = preDeterminedValue[col];
    }
    return tempGain;
}

double Builder::GetSplitGain(int col, int attr){
    int** cols = catCount[col];
    int sum = cols[attr][0] + cols[attr][1];
    int all = 0;
    int success = 0;
    int fail = 0;
    double result1 = 0;
    double result2 = 0;
    for(int i = 0; i < Categories[col]; i++){
        //Calculate GainRatio here
        if(i == attr)continue;
        success += cols[i][1];
        fail += cols[i][0];
    }
    all = success + fail;

    if(sum > 0){
        double div1 = (double)cols[attr][0] / (double) sum;
        double div2 = (double)cols[attr][1] / (double) sum;
        if(div1 > 0){
            result1 -= div1 * log2(div1);
        }
        if(div2 > 0){
            result1 -= div2 * log2(div2);
        }

        result1 = (double)sum * result1;
    }
    if(all > 0){
        double div1 = (double)success / (double)all;
        double div2 = (double)fail / (double)all;
        if(div1 > 0){
            result2 -= div1 * log2(div1);
        }
        if(div2 > 0){
            result2 -= div2 * log2(div2);
        }

        result2 = (double)all * result2;
    }

    double final = ( result1 + result2 ) / (double)( sum + all );
    return final;
}

void Builder::StartBuilding(){
//    std::cout << std::this_thread::get_id() <<" Started\n";
    if(Data->size() == 0){
//        std::cout << "Zero\n";
        node = new DecTree(0, 0, false, true, parent);
        return;
    }
    info = GetInfo();
    int trueCount = 0;
    for(int i = 0; i < colSize; i++){
        if(usedData[i]){
            trueCount++;
        }
    }

    if(trueCount == 0){
//        std::cout << "All Used\n";
        node = new DecTree(0, 0, false, true, dominantAnswer);
        return;
    }

    bool end = ( allFail == 0 ) || ( allSuccess == 0 );
    bool ans = false;
    if(end){
//        std::cout << "End\n";
        if( allFail > 0 ){
            ans = false;
        }
        else{
            ans = true;
        }
        node = new DecTree(0,0,false,true,ans);
        return;
    }

    bestSplit = new double[colSize];

    int maxAttr = 0;
    double maxGain = 0;
    double temp;
    for(int i = 0; i < colSize - 1; i++){
        if(usedData[i]){
            temp = GetGainInfo(i);
            if(temp > maxGain){
                maxAttr = i;
                maxGain = temp;
            }
        }
    }

//    if(maxGain == 0){
//        for(int i = 0; i < colSize; i++){
//            std::cout << i << " used " << usedData[i] << "\n";
//            for(int j = 0; j < Categories[i]; j++){
//                std::cout << j << " : " << catCount[i][j][0] << " " << catCount[i][j][1] << "\n";
//            }
//            if(usedData[i]){
//                if(discrete[i])
//                std::cout << "Gain: " << GetSplitGain(i, bestSplit[i]) << "\n";
//                else std::cout << "Gain: " << GetSplitGain(i, 0) << "\n";
//            }
//        }
//        std::cout << "Info : " << info << "\n";
//        std::cout << "False: " << allFail << " True: " << allSuccess << "\n";
//        std::terminate();
//    }

    if(maxGain == 0){
        for(int i = 0; i < colSize; i++){
            if(usedData[i]){
                maxAttr = i;
                break;
            }
        }
    }

    node = new DecTree(maxAttr, bestSplit[maxAttr], discrete[maxAttr], end, ans);

    //Create new builder for new node

    bool* newRule = new bool[colSize];
    for(int i = 0; i < colSize; i++){
        newRule[i] = usedData[i];
    }
    newRule[maxAttr] = false;
//    delete[] usedData;

    std::vector<double*>* newData1 = new std::vector<double*>();
    std::vector<double*>* newData0 = new std::vector<double*>();
    int size = Data->size();

    for(int i = 0; i < size; i++){
        double* now = (*Data)[i];
        if(discrete[maxAttr]){
            if(now[maxAttr] == bestSplit[maxAttr]){
                newData1->push_back(now);
            }
            else{
                newData0->push_back(now);
            }
        }
        else{
            if(now[maxAttr] > bestSplit[maxAttr]){
//                for(int k = 0; k < 14; k++){
//                    std::cout << now[k] << " ";
//                }
                (*newData1).push_back(now);
            }
            else{
                newData0->push_back(now);
            }
        }
    }

    //Cleaning unused data
    for(int i = 0; i < colSize; i++){
        for(int j = 0; j < Categories[i]; j++){
            delete[] catCount[i][j];
        }
        delete[] catCount[i];
    }
    delete[] catCount;
    delete[] bestSplit;

    Builder* newBuild = new Builder[2];
    newBuild[0].SetData( newData0 );
    newBuild[1].SetData( newData1 );
    for(int i = 0; i < 2; i++){
        newBuild->SetColSize(colSize);
        newBuild[i].SetDeterminedValue(preDeterminedValue);
        newBuild[i].SetDiscretion(discrete);
        newBuild[i].SetUsedCol(newRule);
        newBuild[i].SetParentDominant(dominantAnswer);
        newBuild[i].SetCategoriesSize(Categories);
        newBuild[i].BuildTree();
    }

    for(int i = 0; i < 2; i++){
        node->addChild(newBuild[i].Join());
    }

}

void Builder::SetCategoriesSize(int *Categories){
    this->Categories = Categories;
}

#ifndef DATAREADER_H
#define DATAREADER_H
#include <vector>
#include <string>

class DataReader
{
public:
    DataReader(int colSize = 14);
//    ~DataReader();
    void ReadFile(std::string path, char delim = ',');
    std::vector<double*>* GetData();
    std::vector<std::vector<double>*>* GetColSeperated();
    int GetRowSize();
    int GetColSize();
    double* GetRowAt(int row);
    double GetDataAt(int row, int col);
    double* GetMaxValue();
    double* GetMinValue();
private:
    void Normalize();
    double* SplitNumber(std::string* line, char delim);
    int colSize;
    std::vector<double*> Data;
    std::vector<std::vector<double>*> ColPoint;
    double* maxValue;
    double* minValue;
};

#endif // DATAREADER_H

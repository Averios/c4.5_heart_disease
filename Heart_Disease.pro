TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11
LIBS += -pthread
QMAKE_CXXFLAGS += -Wall

SOURCES += main.cpp \
    datareader.cpp \
    dectree.cpp \
    builder.cpp

HEADERS += \
    datareader.h \
    dectree.h \
    builder.h

